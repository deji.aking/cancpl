stages:
  - setup     # Handle all pipeline jobs that setup the testing
  - merge     # Test merging with any some branches
  - compile   # Compile the fully coupled and atmosphere only configurations
  - run       # Actually run the model
  - answers   # Check the results of the model integration against stored answers
  - document  # Build the documentation
  - cleanup   # Remove any extraneous pipeline data

before_script:
  - export CANCPL_SRC=$CI_PROJECT_DIR
  - echo $CI_PROJECT_DIR $CANCPL_SRC
  - PIPELINE_CACHE=/fs/xc3lustre/dev/eccc/crd/ccrn_tmp/canam_pipeline_$CI_PIPELINE_ID
  - export PIPELINE_CACHE
  - export CONFIGURATION_REPO=$PIPELINE_CACHE/canesm_configurations
  - UPSTREAM=git@gitlab.science.gc.ca:ras003
  - git pull --no-edit $UPSTREAM/CanCPL develop_canesm
  - test -d $PIPELINE_CACHE/CanESM5         && export CANESM5_SRC=$PIPELINE_CACHE/CanESM5    
  - test -d $PIPELINE_CACHE/canesm_configurations && export CONFIG_DIR=$PIPELINE_CACHE/canesm_configurations  

# Clone in or perform any other activities that the rest of the pipeline will need
setup:clone_dependencies:
  stage: setup
  script:
    - mkdir -p $PIPELINE_CACHE
    - cd $PIPELINE_CACHE
    - rm -rf $PIPELINE_CACHE/CanESM5
    - rm -rf $PIPELINE_CACHE/canesm_configurations
    - git clone --recursive $UPSTREAM/CanESM5.git CanESM5
    - git clone $UPSTREAM/canesm_configurations.git canesm_configurations

# Check whether there are any conflicts with the upstream branches
merge:develop_canesm:
  stage: merge
  variables:
    BRANCH_NAME: develop_canesm
  script: &merge_script
    - git pull --no-edit $UPSTREAM/CanCPL.git $BRANCH_NAME

#merge:canam_gem:
#  stage: merge
#  variables:
#    BRANCH_NAME: canam_gem 
#  script: *merge_script
#
#merge:canam_gem_integration:
#  stage: merge
#  variables:
#    BRANCH_NAME: canam_gem_integration
#  script: *merge_script

# Test the compilation of AGCM-only builds
compile:spbc-agcm:32bit:
  stage: compile
  variables:
    CONFIG_NAME: "spbc-agcm"
    CPPDEF:      "AMIP/cppdefs/cppdefs_32bit_amip"
    AGCM_32BIT:  "1"
    AGCM_EXE:    "AGCM.spbc-agcm.32bit"
    CPL_EXE:     "CPL.spbc-agcm.32bit"
  script: &compile_script
    # Prepare for compilation
    - CONFIG_PATH=$CONFIG_DIR/$CONFIG_NAME
    - mkdir -p $CONFIG_PATH/run_directory/executables
    - cd $CANESM5_SRC; rm -rf CanCPL; cp -rf $CANCPL_SRC CanCPL
    # Build CanAM
    - cd CanAM/build
    - cp $CANESM5_SRC/CONFIG/$CPPDEF ./cppdef_config.h
    - cp $CONFIG_PATH/cppdefs/cppdef_sizes.h ./
    - source comp_env; make AGCM_EXE=$AGCM_EXE AGCM32_BIT=$AGCM_32BIT DEBUG=1
    - cp $AGCM_EXE $CONFIG_PATH/run_directory/executables
    # Build CanCPL
    - cd $CANESM5_SRC/CanCPL/build
    - cp $CANESM5_SRC/CONFIG/$CPPDEF ./cppdef_config.h
    - cp $CONFIG_PATH/cppdefs/cppdef_sizes.h ./
    - source comp_env; make CPL_EXE=$CPL_EXE DEBUG=1
    - cp $CPL_EXE $CONFIG_PATH/run_directory/executables

compile:spbc-agcm:64bit:
  stage: compile
  variables:
    CONFIG_NAME: "spbc-agcm"
    CPPDEF:      "AMIP/cppdefs/cppdefs_64bit_amip"
    AGCM_32BIT:  "0"
    AGCM_EXE:    "AGCM.spbc-agcm.64bit"
    CPL_EXE:     "CPL.spbc-agcm.64bit"
  script: *compile_script

  # Test the compilation of fully-coupled builds
compile:pictrl:32bit:
  stage: compile
  variables:
    CONFIG_NAME: "pictrl"
    CPPDEF:      "ESM/cppdefs/cppdefs_32bit_picontrol"
    AGCM_32BIT:  "1"
    AGCM_EXE:    "AGCM.pictrl.32bit"
    CPL_EXE:     "CPL.pictrl.32bit"
  script: *compile_script

compile:pictrl:64bit:
  stage: compile
  variables:
    CONFIG_NAME: "pictrl" 
    AGCM_32BIT:  "0"
    CPPDEF:      "ESM/cppdefs/cppdefs_64bit_picontrol"
    AGCM_EXE:    "AGCM.pictrl.64bit"
    CPL_EXE:     "CPL.pictrl.64bit"
  script: *compile_script

run:integrate:
  stage: run
  variables:
    CONFIGURATIONS: "spbc-agcm.32bit"
  script:    
    - qsub -Wblock=true -v CONFIG_DIR,CONFIGURATIONS .testing/qsub_integrate.sh
    - if [ $? -ne 0 ]; then cat CanAM_CI.* ; fi

answers:spbc-agcm.32bit:
  stage: answers 
  variables:
    CONFIGURATION: "spbc-agcm.32bit"
  script:    
    - cd $CONFIG_DIR/spbc-agcm/answers 
    - git diff $CONFIGURATION
    - if [ $? -ne 0 ]; then git diff $CONFIGURATION; fi

## Doxygen
pages:
  stage: document
  image: ubuntu:latest
  script:
  - apt update
  - apt install doxygen perl texlive-latex-base -y -f -m
  - doxygen docs/Doxyfile
  - mv docs/documentation/html/ public/
  artifacts:
    paths:
    - public
  allow_failure: true

# Cleanup stages
remove_auxiliary:
  stage: cleanup
  script:
    - rm -rf $PIPELINE_CACHE
