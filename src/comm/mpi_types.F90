!> Defines a derived type with procedures and parameters relating to components of CanESM5
module mpi_types_mod

use mpi

implicit none; private

integer(kind=MPI_INTEGER_KIND) :: dummy

! Define enums used to identify model components
integer(kind=MPI_INTEGER_KIND), parameter :: id_coupler = 1
integer(kind=MPI_INTEGER_KIND), parameter :: id_ocean   = 2
integer(kind=MPI_INTEGER_KIND), parameter :: id_atmosphere = 3

!> Define the task group properties for a given component
type group_info_t
  integer(kind=MPI_INTEGER_KIND) :: communicator = MPI_COMM_NULL !< The communicator, NULL on ranks not part of the group.
  integer(kind=MPI_INTEGER_KIND) :: id = -1                    !< The group id based on the enums
  integer(kind=MPI_INTEGER_KIND) :: leader = HUGE(dummy)       !< The leader of the group based on MPI_COMM_WORLD rank
  integer(kind=MPI_INTEGER_KIND), allocatable, dimension(:) :: global_ranks !< Ranks within the MPI_COMM_WORLD

  contains

  procedure :: initialize => initialize_group
  procedure :: broadcast => broadcast_group
  procedure :: assignment_group
  generic   :: assignment(=) => assignment_group

end type group_info_t

public :: group_info_t
public :: id_coupler, id_ocean, id_atmosphere

contains

!> Routine that populates a group_info structure with information about members of a given component
subroutine initialize_group( self, group_comm, group_id )
  class(group_info_t)      , intent(inout) :: self !<  The instance of group_info_t to be populated
  integer(kind=MPI_INTEGER_KIND), intent(in   ) :: group_comm !< The group communicator
  integer                  , intent(in   ) :: group_id   !< The group identifier

  integer(kind=MPI_INTEGER_KIND) :: comm_size, rank, ierr

  self%id = group_id
  self%communicator = group_comm

  call MPI_COMM_RANK( MPI_COMM_WORLD, rank, ierr )
  call MPI_ALLREDUCE( rank, self%leader, 1, MPI_INTEGER, MPI_MIN, self%communicator, ierr )

  call MPI_COMM_SIZE( self%communicator, comm_size, ierr )
  call MPI_COMM_RANK( MPI_COMM_WORLD, rank, ierr )

  ALLOCATE(self%global_ranks(comm_size))
  call MPI_ALLGATHER( rank, 1, MPI_INTEGER, self%global_ranks, 1, MPI_INTEGER, self%communicator, ierr )

end subroutine initialize_group

!> Broadcast group information to all processes
subroutine broadcast_group(self, broadcaster)
  class(group_info_t),       intent(inout) :: self        !< The specific instance of group_info_t
  integer(kind=MPI_INTEGER_KIND), intent(in   ) :: broadcaster !< The rank to broadcast information from

  integer(kind=MPI_INTEGER_KIND) :: ierr, rank, nranks
  logical :: is_broadcaster

  call MPI_COMM_RANK( MPI_COMM_WORLD, rank, ierr )

  is_broadcaster = .FALSE.
  if (rank == broadcaster) is_broadcaster = .TRUE.

  ! Broadcast fixed size fields
  call MPI_BCAST( self%id,     1, MPI_INTEGER, broadcaster, MPI_COMM_WORLD, ierr )
  call MPI_BCAST( self%leader, 1, MPI_INTEGER, broadcaster, MPI_COMM_WORLD, ierr )
  ! DO NOT BROADCAST THE COMMUNICATOR. For some reason, the communicator does not have the same value on all processes
  ! within the group
  ! call MPI_BCAST( self%communicator, 1, MPI_INTEGER, broadcaster, MPI_COMM_WORLD, ierr )

  ! Broadcast variable size fields, allocating memory as needed
  nranks = SIZE(self%global_ranks)
  call MPI_BCAST( nranks, 1, MPI_INTEGER, broadcaster, MPI_COMM_WORLD, ierr )

  IF (.NOT. is_broadcaster) then
    IF (ALLOCATED(self%global_ranks)) DEALLOCATE(self%global_ranks)
    ALLOCATE( self%global_ranks(nranks) )
  ENDIF
  call MPI_BCAST( self%global_ranks, nranks, MPI_INTEGER, broadcaster, MPI_COMM_WORLD, ierr )

end subroutine broadcast_group

!> Define the assignment operator for the group type by copying all internal fields
subroutine assignment_group( other, self )
  class(group_info_t), intent(  out) :: other !< The group to be copied to
  class(group_info_t), intent(in   ) :: self  !< The group to be copied

  other%communicator = self%communicator
  other%id           = self%id
  other%leader       = self%leader

  if (allocated(other%global_ranks)) deallocate(other%global_ranks)

  if (allocated(self%global_ranks)) then
    allocate(other%global_ranks( size(self%global_ranks)) )
    other%global_ranks(:) = self%global_ranks(:)
  endif
end subroutine assignment_group

end module mpi_types_mod